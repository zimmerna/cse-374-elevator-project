public class main {
    public static void main(String[] args) {
        System.out.println("Unit Test 0 - Base Case");
        int[][] passengers = {{1,2,0}};      
        int pscan = Elevator_Model.PSCAN(1, passengers);
        int fcfs = Elevator_Model.FCFS(1, passengers);
        int sstf = Elevator_Model.SSTF(1, passengers);
        System.out.println("all PSCAN passengers serviced by time " + pscan);
        System.out.println("all FCFS passengers serviced by time " + fcfs);
        System.out.println("all SSTF passengers serviced by time " + sstf);

        System.out.println("Unit Test 1 - Sudden Rush");
        int[][] passengers1 = {{1,2,0}, {3,5,0}, {4,2,0}, {2,6,0}, {5,9,0}, {7,1,0}};  
        int pscan1 = Elevator_Model.PSCAN(1, passengers1);
        int fcfs1 = Elevator_Model.FCFS(1, passengers1);
        int sstf1 = Elevator_Model.SSTF(1, passengers1);
        System.out.println("all PSCAN passengers serviced by time " + pscan1);
        System.out.println("all FCFS passengers serviced by time " + fcfs1);
        System.out.println("all SSTF passengers serviced by time " + sstf1);

        System.out.println("Unit Test 2 - The Twenty Floors");
        int[][] passengers2 = {{1,2,0}, {1,2,1}, {1,2,2}, {1,2,3}, {20,1,3}, {2,1,4}, {1,2,5}, {2,1,6}, {1,2,7}, {2,1,8}, {1,2,9}, {2,1,10},{1,2,11},{1,2,12},
                                {1,2,13}, {2,1,14},{1,2,15}, {2,1,16},{1,2,17}, {2,1,18},{1,2,19}, {2,1,20}};  
        int pscan2 = Elevator_Model.PSCAN(1, passengers2);
        int fcfs2 = Elevator_Model.FCFS(1, passengers2);
        int sstf2 = Elevator_Model.SSTF(1, passengers2);
        System.out.println("all PSCAN passengers serviced by time " + pscan2);
        System.out.println("all FCFS passengers serviced by time " + fcfs2);
        System.out.println("all SSTF passengers serviced by time " + sstf2);
    }
}
