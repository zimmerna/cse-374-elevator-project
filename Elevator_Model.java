import java.util.*;

public class Elevator_Model {
    static int PSCAN(int floor, int P[][]) {
        // just some stuff to make the tables look nice
        int passLength = (P.length * 9);
        if (passLength > 100) {
            passLength = 100;
        }

        String format = "%s %" + passLength + "s %s";
        String equals = "";
        
        for (int i = 0; i < 63 + passLength; i++){
            equals = equals + "=";
        }
        System.out.println("                                ┌─────────┐");
        System.out.println("                                │  PSCAN  │");
        System.out.println("                                └─────────┘");
        System.out.println(equals);                                        
        System.out.format(format, "| time | floor | direction |    action |          requests |", "passengers", "|\n");
        System.out.println(equals);

        
        int time = 0;
        int direction = 0;
        Vector<Integer> requests = new Vector<Integer>();
        Vector<Passenger> passengers = new Vector<Passenger>();
        for (int[] array_passenger : P) {
            Passenger passenger = new Passenger(array_passenger);
            passengers.add(passenger);
        }

        printTickStatus(time, floor, direction, "N/A", requests, passengers, passLength);
        time++;
        while (!requests.isEmpty() || !passengers.isEmpty()) {
            for (Passenger p : passengers) {
                if (p.time_appears <= time && !requests.contains(p.source)) {
                    requests.add(p.source);
                }
            }

            if (requests.contains(floor)) {
                while (requests.contains(floor)) {
                    requests.remove(requests.indexOf(floor));
                }
                Vector<Passenger> removable = new Vector<Passenger>();
                for (Passenger p : passengers) {
                    if ((p.source == floor) && (p.time_appears <= time)) {
                        if (!requests.contains(p.destination)) {
                            requests.add(p.destination);
                        }
                        removable.add(p);
                    }
                }
                for (Passenger r : removable) {
                    passengers.remove(r);
                }
                printTickStatus(time, floor, direction, "servicing", requests, passengers, passLength);
                time++;
            } else {
                if (requests.isEmpty()) {
                    direction = 0;
                } else if (direction == 0) {
                    int g = findClosestRequest(floor, requests);  
                    direction = g < floor ? -1 : 1;
                } else {
                    boolean changeDirection = true;
                    for (int r : requests) {
                        if (!((direction == -1 && r > floor) || (direction == 1 && r < floor))) {
                            changeDirection = false;
                        }
                    }
                    if (changeDirection) {
                        direction = -direction;
                    }
                }
                floor = floor + direction;
                printTickStatus(time, floor, direction, "moving", requests, passengers, passLength);
                time++;
            }
        }
        System.out.println(equals);
        return time-1;
    }

    static int FCFS(int floor, int P[][]) {
        // just some stuff to make the tables look nice
        int passLength = (P.length * 9);
        if (passLength > 100) {
            passLength = 100;
        }
        String format = "%s %" + passLength + "s %s";
        String equals = "";
        
        for (int i = 0; i < 65 + passLength; i++){
            equals = equals + "=";
        }
        System.out.println("                                ┌────────┐");
        System.out.println("                                │  FCFS  │");
        System.out.println("                                └────────┘");
        System.out.println(equals);
        System.out.format(format, "| time | floor | destination |    action |          requests |", "passengers", "|\n");
        System.out.println(equals);

        int time = 0;
        int destination = -1;
        Vector<Integer> requests = new Vector<Integer>();
        Vector<Passenger> passengers = new Vector<Passenger>();
        for (int[] array_passenger : P) {
            Passenger passenger = new Passenger(array_passenger);
            passengers.add(passenger);
        }

        printTickStatus2(time, floor, -1, "N/A", requests, passengers, passLength);
        time++;

        while (!requests.isEmpty() || !passengers.isEmpty()) {
            Vector<Integer> addable = new Vector<Integer>();
            for (Passenger p : passengers) {
                if (p.time_appears <= time && !requests.contains(p.source)) {
                    if (!addable.contains(p.source)) {
                        addable.add(p.source); 
                    }
                }
            }
            while (!addable.isEmpty()) {
                Integer toAdd = findClosestRequest(floor, addable);
                requests.add(toAdd);
                addable.remove(toAdd);
            }

            if (destination == -1) {
                destination = requests.firstElement();
            }

            if ((floor == destination) && (requests.contains(floor))) {
                while (requests.contains(floor)) {
                    requests.remove(requests.indexOf(floor));
                }
                Vector<Passenger> removable = new Vector<Passenger>();
                for (Passenger p : passengers) {
                    if ((p.source == floor) && (p.time_appears <= time)) {
                        if (!requests.contains(p.destination) ) {
                            if (!addable.contains(p.destination)) {
                                addable.add(p.destination); 
                            }
                        }
                        removable.add(p);
                    }
                }
                
                for (Passenger r : removable) {
                    passengers.remove(r);
                }
                while (!addable.isEmpty()) {
                    Integer toAdd = findClosestRequest(floor, addable);
                    requests.add(toAdd);
                    addable.remove(toAdd);
                }
                printTickStatus2(time, floor, destination, "servicing", requests, passengers, passLength);
                if (!requests.isEmpty()) {
                    destination = requests.firstElement();
                } else {
                    destination = -1;
                }
                time++;
            } else {
                if (!requests.isEmpty()) {
                    floor = (destination < floor) ? floor-1 : floor+1; 
                    printTickStatus2(time, floor, destination, "moving", requests, passengers, passLength);
                    time++;
                }
            }
        }
        System.out.println(equals);
        return time-1;
    }

    static int SSTF(int floor, int P[][]) {
        // just some stuff to make the tables look nice
        int passLength = (P.length * 9);
        if (passLength > 100) {
            passLength = 100;
        }
        String format = "%s %" + passLength + "s %s";
        String equals = "";
        
        for (int i = 0; i < 65 + passLength; i++){
            equals = equals + "=";
        }
        System.out.println("                                ┌────────┐");
        System.out.println("                                │  SSTF  │");
        System.out.println("                                └────────┘");
        System.out.println(equals);
        System.out.format(format, "| time | floor | destination |    action |          requests |", "passengers", "|\n");
        System.out.println(equals);

        int time = 0;
        int destination = -1;
        Vector<Integer> requests = new Vector<Integer>();
        Vector<Passenger> passengers = new Vector<Passenger>();
        for (int[] array_passenger : P) {
            Passenger passenger = new Passenger(array_passenger);
            passengers.add(passenger);
        }

        printTickStatus2(time, floor, -1, "N/A", requests, passengers, passLength);
        time++;

        while (!requests.isEmpty() || !passengers.isEmpty()) {
            for (Passenger p : passengers) {
                if (p.time_appears <= time && !requests.contains(p.source)) {
                    requests.add(p.source);
                }
            }

            if (destination == -1) {
                destination = findClosestRequest(floor, requests); // the only changed line between this and FCFS
            }

            if ((floor == destination) && (requests.contains(floor))) {
                while (requests.contains(floor)) {
                    requests.remove(requests.indexOf(floor));
                }
                Vector<Passenger> removable = new Vector<Passenger>();
                for (Passenger p : passengers) {
                    if ((p.source == floor) && (p.time_appears <= time)) {
                         if (!requests.contains(p.destination)) {
                            requests.add(p.destination);
                         }
                         removable.add(p);
                    }
                }
                for (Passenger r : removable) {
                    passengers.remove(r);
                }
                printTickStatus2(time, floor, destination, "servicing", requests, passengers, passLength);
                if (!requests.isEmpty()) {
                    destination = findClosestRequest(floor, requests); // this too
                } else {
                    destination = -1;
                }
                time++;
            } else {
                if (!requests.isEmpty()) {
                    floor = (destination < floor) ? floor-1 : floor+1; 
                    printTickStatus2(time, floor, destination, "moving", requests, passengers, passLength);
                    time++;
                }
            }
        }
        System.out.println(equals);
        return time-1;
    }


    private static void printTickStatus(int time, int floor, int direction, String action, Vector<Integer> requests, Vector<Passenger> passengers, int pLength) {
        String format = "| %4d | %5d | %9d | %9s | %17s | %" + pLength + "s | \n";
        String passString = passengers.toString();
        if (passengers.size() > 10) {
            passString = passString.substring(0, 92) + "...";
        }
        System.out.format(format, time, floor, direction, action, requests.toString(), passString);
    }

    private static void printTickStatus2(int time, int floor, int destination, String action, Vector<Integer> requests, Vector<Passenger> passengers, int pLength) {
        String format = "| %4d | %5d | %11d | %9s | %17s | %" + pLength + "s | \n";
        String passString = passengers.toString();
        if (passengers.size() > 10) {
            passString = passString.substring(0, 92) + "...";
        }
        System.out.format(format, time, floor, destination, action, requests.toString(), passString);
    }

    private static int findClosestRequest(int f, Vector<Integer> requests) {
        int min = -1;
        int min_distance = Integer.MAX_VALUE;
        for (int r : requests) {
            int distance = Math.abs(f-r);
            if (distance < min_distance) {
                min = r;
                min_distance = distance;
            }
        }
        return min;
    }

    private static class Passenger {
        private int source;
        private int destination;
        private int time_appears;

        public Passenger(int[] p) {
            source = p[0];
            destination = p[1];
            time_appears = p[2];
        }

        public String toString() {
            String result = "[" + source + "," + destination + "," + time_appears + "]";
            return result;
        }
    }
}
